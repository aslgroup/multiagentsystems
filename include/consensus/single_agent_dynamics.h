#include <ros/ros.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <Eigen/Dense>
#include <Eigen/Geometry>

class SingleAgentDynamics{
private:
	int num_of_agents;
	float offset_distance_value;

	ros::NodeHandle nh;
	ros::Subscriber agentOnePositionSub;
	ros::Subscriber agentTwoPositionSub;
	ros::Publisher agentOnePositionPub;
	ros::Publisher agentTwoPositionPub;
	//ros::Publisher agentOneVelocityPub;
	//ros::Publisher agentTwoVelocityPub;
	ros::Publisher actualDltaPub;
	ros::Publisher requiredDeltaPub;

	Eigen::Vector3f agentOne_pos;
	Eigen::Vector3f agentTwo_pos;
	Eigen::Quaternion<float> agentOne_orientation;
	Eigen::Quaternion<float> agentTwo_orientation;

	Eigen::MatrixXf A;
	Eigen::MatrixXf D;
	Eigen::MatrixXf L;

	Eigen::Vector3f offset_distance;

	geometry_msgs::PoseStamped msg;

	std::string actual_agent_one_position_topic_name;
	std::string actual_agent_two_position_topic_name;
	std::string new_agent_one_position_topic_name;
	std::string new_agent_two_position_topic_name;
	std::string delta_position_between_agents_topic_name;
	std::string required_delta_position_between_agents_topic_name;
	double rate_to_publish;

public:
	SingleAgentDynamics();
	~SingleAgentDynamics();

	void agentOnePosition_cb(const geometry_msgs::PoseStamped& msg);
	void agentTwoPosition_cb(const geometry_msgs::PoseStamped& msg);

	bool load_param();
	void run();
};
