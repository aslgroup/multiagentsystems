#include <consensus/single_agent_dynamics.h>

SingleAgentDynamics::SingleAgentDynamics()
{
	load_param();
	num_of_agents = 2;
	offset_distance_value = 50;

	A = Eigen::MatrixXf(num_of_agents,num_of_agents);
	D = Eigen::MatrixXf(num_of_agents,num_of_agents);

	A.row(0) << 0, 1;
	A.row(1) << 1, 0;
	D.row(0) << 1, 0;
	D.row(1) << 0, 1;

	std::cout << "A:\n" << A << "\n\n";
	std::cout << "D:\n" << D << "\n\n";

	L = -D+A;

	std::cout << "L:\n" << L << "\n\n";

	offset_distance = Eigen::Vector3f(offset_distance_value,offset_distance_value,offset_distance_value);

	agentOnePositionSub = nh.subscribe(actual_agent_one_position_topic_name, 1, &SingleAgentDynamics::agentOnePosition_cb, this);
	agentTwoPositionSub = nh.subscribe(actual_agent_two_position_topic_name, 1, &SingleAgentDynamics::agentTwoPosition_cb,this);
    agentOnePositionPub = nh.advertise<geometry_msgs::PoseStamped>(new_agent_one_position_topic_name,1);
    agentTwoPositionPub = nh.advertise<geometry_msgs::PoseStamped>(new_agent_two_position_topic_name,1);
    actualDltaPub = nh.advertise<geometry_msgs::Pose>(delta_position_between_agents_topic_name,1);
    requiredDeltaPub = nh.advertise<geometry_msgs::Pose>(required_delta_position_between_agents_topic_name,1);
}

SingleAgentDynamics::~SingleAgentDynamics()
{

}

bool SingleAgentDynamics::load_param()
{

	if (!nh.getParam("DoubleAgentDynamics/actual_agent_one_position_topic_name", actual_agent_one_position_topic_name )){ ROS_ERROR("Value not loaded from parameter: %s !)", actual_agent_one_position_topic_name.c_str()) ; return false; }
	if (!nh.getParam("DoubleAgentDynamics/actual_agent_two_position_topic_name", actual_agent_two_position_topic_name )){ ROS_ERROR("Value not loaded from parameter: %s !)", actual_agent_two_position_topic_name.c_str()) ; return false; }
	if (!nh.getParam("DoubleAgentDynamics/new_agent_one_position_topic_name", new_agent_one_position_topic_name )){ ROS_ERROR("Value not loaded from parameter: %s !)", new_agent_one_position_topic_name.c_str()) ; return false; }
	if (!nh.getParam("DoubleAgentDynamics/new_agent_two_position_topic_name", new_agent_two_position_topic_name )){ ROS_ERROR("Value not loaded from parameter: %s !)", new_agent_two_position_topic_name.c_str()) ; return false; }
	if (!nh.getParam("DoubleAgentDynamics/delta_position_between_agents_topic_name", delta_position_between_agents_topic_name )){ ROS_ERROR("Value not loaded from parameter: %s !)", delta_position_between_agents_topic_name.c_str()) ; return false; }
	if (!nh.getParam("DoubleAgentDynamics/required_delta_position_between_agents_topic_name", required_delta_position_between_agents_topic_name )){ ROS_ERROR("Value not loaded from parameter: %s !)", required_delta_position_between_agents_topic_name.c_str()) ; return false; }
	if (!nh.getParam("DoubleAgentDynamics/rate_to_publish", rate_to_publish)){ ROS_INFO("Default to 100 Hz"); rate_to_publish=100;}


}

void SingleAgentDynamics::agentOnePosition_cb(const geometry_msgs::PoseStamped& msg)
{
	agentOne_pos(0) = msg.pose.position.x;
	agentOne_pos(1) = msg.pose.position.y;
	agentOne_pos(2) = msg.pose.position.z;
	agentOne_orientation = Eigen::Quaternion<float>(msg.pose.orientation.w,msg.pose.orientation.x,msg.pose.orientation.y,msg.pose.orientation.z);
	return;
}

void SingleAgentDynamics::agentTwoPosition_cb(const geometry_msgs::PoseStamped& msg)
{
	agentTwo_pos(0) = msg.pose.position.x;
	agentTwo_pos(1) = msg.pose.position.y;
	agentTwo_pos(2) = msg.pose.position.z;
	agentTwo_orientation = Eigen::Quaternion<float>(msg.pose.orientation.w,msg.pose.orientation.x,msg.pose.orientation.y,msg.pose.orientation.z);
	return;
}

void SingleAgentDynamics::run()
{
	Eigen::Vector3f actual_delta_agentOne_agentTwo = agentTwo_pos-agentOne_pos;
	Eigen::Vector3f required_delta_agentOne_agentTwo = actual_delta_agentOne_agentTwo - offset_distance;
	Eigen::Vector3f new_desired_position_agentTwo = agentTwo_pos + required_delta_agentOne_agentTwo;

	/**Publish new position values**/
	msg.header.frame_id = "";
	msg.header.stamp = ros::Time();
	msg.pose.position.x = new_desired_position_agentTwo(0);
	msg.pose.position.y = new_desired_position_agentTwo(1);
	msg.pose.position.z = new_desired_position_agentTwo(2);

	agentTwoPositionPub.publish(msg);

	msg.pose.position.x = actual_delta_agentOne_agentTwo(0);
	msg.pose.position.y = actual_delta_agentOne_agentTwo(1);
	msg.pose.position.z = actual_delta_agentOne_agentTwo(2);

	actualDltaPub.publish(msg.pose);

	msg.pose.position.x = required_delta_agentOne_agentTwo(0);
	msg.pose.position.y = required_delta_agentOne_agentTwo(1);
	msg.pose.position.z = required_delta_agentOne_agentTwo(2);

	requiredDeltaPub.publish(msg.pose);
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "single_agent_dynamics");

	SingleAgentDynamics SAD;

	while(ros::ok())
	{
		ros::spinOnce();
		//SAD.run();
	}

  return 0;
}
